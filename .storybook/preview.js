import { addParameters, addDecorator } from '@storybook/react';
import { withA11y } from '@storybook/addon-a11y';

addParameters({
  options: {
    panelPosition: 'right'
  }
});

addDecorator(withA11y);
