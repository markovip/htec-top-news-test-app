import React from 'react';
import Dummy from '../src/components/Dummy';

export default {
  title: 'Dummy',
  component: Dummy
};

export const dummyStory = () => <Dummy text="Dummy Story" />;
