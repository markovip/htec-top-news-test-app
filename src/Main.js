import React from 'react';
import { Route, Switch } from 'react-router-dom';
import TopNews from './components/TopNews';
import Category from './components/Category';
import Search from './components/Search';
import NewsDetail from './components/NewsDetails';
import SpecificCategory from './components/SpecificCategory';

export default function Main () {
  return (
    <Switch>
      <Route exact path="/" component={TopNews} />
      <Route path="/categories" exact component={Category} />
      <Route path="/categories/:category" component={SpecificCategory} />
      <Route path="/search" exact component={Search} />
      <Route path="/:title" component={NewsDetail} />
    </Switch>
  );
}
