import React, { Fragment } from 'react';
import Header from './Header';
import Main from './Main';

export default function App () {
  return (
    <Fragment>
      <header className="root__header">
        <Header />
      </header>
      <main className="root__main">
        <Main />
      </main>
      <aside className="root__left"></aside>
      <aside className="root__right"></aside>
      <footer className="root__footer"></footer>
    </Fragment>
  );
}
