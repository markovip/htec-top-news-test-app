import { combineReducers } from 'redux';
import * as a from '../actions';

const topNewsInitialState = {
  isLoading: false,
  articles: [],
  totalResults: 0,
  error: null
};

const categoryNewsInitialState = {
  isLoading: false,
  categories: [],
  error: null
};

const countryInitialState = {
  availableCountries: [
    {
      code: 'gb',
      name: 'Great Britain'
    },
    {
      code: 'us',
      name: 'United States Of America'
    }
  ],
  selectedCountry: {
    code: 'gb',
    name: 'Great Britain'
  }
};

const topNewsReducer = (state = topNewsInitialState, action) => {
  switch (action.type) {
    case a.GET_TOP_HEADLINES:
      return { ...state, isLoading: true };
    case a.GET_TOP_HEADLINES_SUCCESS: {
      const { totalResults, articles } = action.payload;
      return { ...state, isLoading: false, totalResults, articles };
    }
    case a.GET_TOP_HEADLINES_FAILED:
      return state;

    case a.GET_CATEGORY_HEADLINES_ALL:
      return { ...state, isLoading: true };
    case a.GET_CATEGORY_HEADLINES_ALL_SUCCESS: {
      console.log(action.categoryHeadlines);
      const { totalResults, articles } = action.categoryHeadlines;
      return { ...state, isLoading: false, totalResults, articles };
    }
    case a.GET_CATEGORY_HEADLINES_ALL_FAILED:
      return state;

    case a.GET_SEARCH_HEADLINES:
      return state;
    case a.GET_SEARCH_HEADLINES_SUCCESS: {
      const { totalResults, articles } = action.payload;
      return { ...state, totalResults, articles };
    }
    case a.GET_SEARCH_HEADLINES_FAILED:
      return { ...state, error: action.payload };
    default:
      return state;
  }
};

const categoryNewsReducer = (state = categoryNewsInitialState, action) => {
  switch (action.type) {
    case a.GET_CATEGORY_HEADLINES:
      return { ...state, isLoading: true };
    case a.GET_CATEGORY_HEADLINES_SUCCESS:
      return {
        ...state,
        isLoading: false,
        categories: action.categoryHeadlines
      };
    case a.GET_CATEGORY_HEADLINES_FAILED:
      return { ...state, error: action.payload };
    default:
      return state;
  }
};

/* const searchHeadlinesReducer = (
  state = searchHeadlinesInitialState,
  action
) => {
  switch (action.type) {
    case a.GET_SEARCH_HEADLINES:
      return state;
    case a.GET_SEARCH_HEADLINES_SUCCESS: {
      const { totalResults, articles } = action.payload;
      return { ...state, totalResults, articles };
    }
    case a.GET_SEARCH_HEADLINES_FAILED:
      return state;
    case a.CLEAR_SEARCH_HEADLINES_SUCCESS:
      return { ...state, ...searchHeadlinesInitialState };
    default:
      return state;
  }
}; */

const countryReducer = (state = countryInitialState, action) => {
  switch (action.type) {
    case a.SET_COUNTRY:
      return state;
    case a.SET_COUNTRY_SUCCESS:
      return {
        ...state,
        selectedCountry: state.availableCountries.find(
          country => country.code === action.country
        )
      };
    default:
      return state;
  }
};

export default combineReducers({
  topNewsReducer,
  categoryNewsReducer,
  // searchHeadlinesReducer,
  countryReducer
});
