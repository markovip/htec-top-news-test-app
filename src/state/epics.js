import { ofType, combineEpics } from 'redux-observable';
import {
  map,
  switchMap,
  mergeMap,
  debounceTime,
  catchError,
  mapTo
} from 'rxjs/operators';
import { of, forkJoin } from 'rxjs';

import * as a from './actions';
import * as Api from './api';

export const getTopHeadlinesEpic = (action$, state$) =>
  action$.pipe(
    ofType(a.GET_TOP_HEADLINES),
    switchMap(({ country }) =>
      Api.getTopHeadlines(country).pipe(
        map(response => a.getTopHeadlinesSuccess(response)),
        catchError(error =>
          of({
            type: a.GET_TOP_HEADLINES_FAILED,
            payload: error.xhr.response,
            error: true
          })
        )
      )
    )
  );

const getCategoriesHeadlinesEpic = action$ =>
  action$.pipe(
    ofType(a.GET_CATEGORY_HEADLINES),
    switchMap(action$ =>
      forkJoin(
        action$.payload.categories.map(category =>
          Api.getCategoryHeadlines(category, action$.payload.country).pipe(
            map(response => ({ category, response })),
            catchError(error =>
              of({
                type: a.GET_CATEGORY_HEADLINES_FAILED,
                payload: error.xhr.response,
                error: true
              })
            )
          )
        )
      )
    ),
    mergeMap(data => {
      return of(a.getCategoryHeadlinesSuccess(data));
    })
  );

export const getCategoryHeadlinesAllEpic = (action$, state$) =>
  action$.pipe(
    ofType(a.GET_CATEGORY_HEADLINES_ALL),
    switchMap(action$ =>
      Api.getCategoryHeadlinesAll(
        action$.payload.category,
        action$.payload.country
      ).pipe(
        map(response => a.getCategoryHeadlinesAllSuccess(response)),
        catchError(error =>
          of({
            type: a.GET_CATEGORY_HEADLINES_ALL_FAILED,
            payload: error.xhr.response,
            error: true
          })
        )
      )
    )
  );
const searchHeadlinesEpic = action$ =>
  action$.pipe(
    ofType(a.GET_SEARCH_HEADLINES),
    debounceTime(1000),
    switchMap(action =>
      Api.searchHeadlines(action.payload.query, action.payload.country).pipe(
        map(response => a.searchHeadlinesSuccess(response)),
        catchError(error =>
          of({
            type: a.GET_TOP_HEADLINES_FAILED,
            payload: error.xhr.response,
            error: true
          })
        )
      )
    )
  );

const clearSearchHeadlines = action$ =>
  action$.pipe(
    ofType(a.CLEAR_SEARCH_HEADLINES),
    mapTo(a.clearSearchHeadlinesSuccess())
  );

const setCountryEpic = action$ =>
  action$.pipe(
    ofType(a.SET_COUNTRY),
    mapTo(a.setCountrySuccess(action$.country))
  );

export default combineEpics(
  getTopHeadlinesEpic,
  getCategoriesHeadlinesEpic,
  getCategoryHeadlinesAllEpic,
  searchHeadlinesEpic,
  clearSearchHeadlines,
  setCountryEpic
);
