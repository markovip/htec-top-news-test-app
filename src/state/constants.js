const PAGESIZE = 100;
const CATEGORY_PAGESIZE = 5;

// API URL
const API_KEY_VALUE = 'a0ceeffffc294c5787807e43e3922336';
const API_URL = 'https://newsapi.org/v2';
const API_KEY = `?apiKey=${API_KEY_VALUE}`;
const API_PARAMS = `&pageSize=${PAGESIZE}`;
const API_PARAMS_CATEGORY = `&pageSize=${CATEGORY_PAGESIZE}`;

// API End Points
export const TOP_HEADLINES = `${API_URL}/top-headlines${API_KEY}`;
export const TOP_HEADLINES_PAGED = `${API_URL}/top-headlines${API_KEY}${API_PARAMS}`;
export const CATEGORY_HEADLINES = `${API_URL}/top-headlines${API_KEY}${API_PARAMS_CATEGORY}`;
export const CATEGORY_HEADLINES_ALL = `${API_URL}/top-headlines${API_KEY}`;
export const SEARCH = `${API_URL}/everything${API_KEY}${API_PARAMS}&sortBy=relevancy`;

// CATEGORIES
export const CATEGORIES = [
  'Entertainment',
  'General',
  'Health',
  'Science',
  'Sports',
  'Technology'
];
