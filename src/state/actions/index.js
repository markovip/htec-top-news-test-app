export const GET_TOP_HEADLINES = 'GET_TOP_HEADLINES';
export const GET_TOP_HEADLINES_SUCCESS = 'GET_TOP_HEADLINES_SUCCESS';
export const GET_TOP_HEADLINES_FAILED = 'GET_TOP_HEADLINES_FAILED';

export const GET_CATEGORY_HEADLINES = 'GET_CATEGORY_HEADLINES';
export const GET_CATEGORY_HEADLINES_SUCCESS = 'GET_CATEGORY_HEADLINES_SUCCESS';
export const GET_CATEGORY_HEADLINES_FAILED = 'GET_CATEGORY_HEADLINES_FAILED';

export const GET_CATEGORY_HEADLINES_ALL = 'GET_CATEGORY_HEADLINES_ALL';
export const GET_CATEGORY_HEADLINES_ALL_SUCCESS =
  'GET_CATEGORY_HEADLINES_ALL_SUCCESS';
export const GET_CATEGORY_HEADLINES_ALL_FAILED =
  'GET_CATEGORY_HEADLINES_ALL_FAILED';

export const GET_SEARCH_HEADLINES = 'GET_SEARCH_HEADLINES';
export const GET_SEARCH_HEADLINES_SUCCESS = 'GET_SEARCH_HEADLINES_SUCCESS';
export const GET_SEARCH_HEADLINES_FAILED = 'GET_SEARCH_HEADLINES_FAILED';
export const CLEAR_SEARCH_HEADLINES = 'CLEAR_SEARCH_HEADLINES';
export const CLEAR_SEARCH_HEADLINES_SUCCESS = 'CLEAR_SEARCH_HEADLINES_SUCCESS';

export const SET_COUNTRY = 'SET_COUNTRY';
export const SET_COUNTRY_SUCCESS = 'SET_COUNTRY_SUCCESS';

export const getTopHeadlines = country => ({
  type: GET_TOP_HEADLINES,
  country
});

export const getTopHeadlinesSuccess = topHeadlines => ({
  type: GET_TOP_HEADLINES_SUCCESS,
  payload: topHeadlines
});

export const getTopHeadlinesFailed = payload => ({
  type: GET_TOP_HEADLINES_FAILED,
  payload,
  error: true
});

export const getCategoryHeadlines = (categories, country) => ({
  type: GET_CATEGORY_HEADLINES,
  payload: { categories, country }
});

export const getCategoryHeadlinesSuccess = categoryHeadlines => ({
  type: GET_CATEGORY_HEADLINES_SUCCESS,
  categoryHeadlines
});

export const getCategoryHeadlinesFailed = error => ({
  type: GET_CATEGORY_HEADLINES_FAILED,
  payload: error.xhr.response,
  error: true
});

export const getCategoryHeadlinesAll = (category, country) => ({
  type: GET_CATEGORY_HEADLINES_ALL,
  payload: { category, country }
});

export const getCategoryHeadlinesAllSuccess = categoryHeadlines => ({
  type: GET_CATEGORY_HEADLINES_ALL_SUCCESS,
  categoryHeadlines
});

export const getCategoryHeadlinesAllFailed = error => ({
  type: GET_CATEGORY_HEADLINES_ALL_FAILED,
  payload: error.xhr.response,
  error: true
});

export const searchHeadlines = (query, country) => ({
  type: GET_SEARCH_HEADLINES,
  payload: { query, country }
});

export const searchHeadlinesSuccess = searchedHeadlines => ({
  type: GET_SEARCH_HEADLINES_SUCCESS,
  payload: searchedHeadlines
});

export const searchedHeadlinesFailed = payload => ({
  type: GET_SEARCH_HEADLINES_FAILED,
  payload,
  error: true
});

export const clearSearchHeadlines = () => ({
  type: CLEAR_SEARCH_HEADLINES
});

export const clearSearchHeadlinesSuccess = () => ({
  type: CLEAR_SEARCH_HEADLINES
});

export const setCountry = country => {
  return {
    type: SET_COUNTRY_SUCCESS,
    country
  };
};
export const setCountrySuccess = country => ({
  type: SET_COUNTRY_SUCCESS,
  country
});
