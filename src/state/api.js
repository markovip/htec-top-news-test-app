import { ajax } from 'rxjs/ajax';
import * as c from './constants';

export const getTopHeadlines = (country = 'us') => {
  return ajax.getJSON(
    `${c.TOP_HEADLINES_PAGED}&country=${country.toLowerCase()}`
  );
};

export const getCategoryHeadlines = (category, country = 'us') =>
  ajax.getJSON(
    `${
      c.CATEGORY_HEADLINES
    }&country=${country.toLowerCase()}&category=${category.toLowerCase()}`
  );

export const getCategoryHeadlinesAll = (category, country = 'us') =>
  ajax.getJSON(
    `${
      c.CATEGORY_HEADLINES_ALL
    }&country=${country.toLowerCase()}&category=${category.toLowerCase()}`
  );

export const searchHeadlines = (query, country = 'us') => {
  return ajax.getJSON(
    `${c.TOP_HEADLINES_PAGED}&q=${query}&country=${country.toLowerCase()}`
  );
};
