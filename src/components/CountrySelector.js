import React from 'react';
import { useDispatch, useSelector } from 'react-redux';

import { setCountry } from '../state/actions';
const CountrySelector = () => {
  const dispatch = useDispatch();
  const countryReducer = useSelector(({ countryReducer }) => countryReducer);
  const { availableCountries, selectedCountry } = countryReducer;

  const handleCountryOnChange = e => {
    e.preventDefault();
    dispatch(setCountry(e.target.value));
  };
  return (
    <>
      {availableCountries.map((country, index) => {
        return (
          <li key={index}>
            <input
              type="radio"
              id={`radio_${country.code}`}
              value={country.code.toLowerCase()}
              onChange={handleCountryOnChange}
              checked={country.code === selectedCountry.code}
            />
            <label
              htmlFor={`radio_${country.code}`}
              className={
                country.code === selectedCountry.code ? 'selected' : ''
              }
            >
              {country.code.toUpperCase()}
            </label>
          </li>
        );
      })}
    </>
  );
};

export default CountrySelector;
