/* eslint-disable react/prop-types */
import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import styled from 'styled-components';
import { css } from '@emotion/core';
import GridLoader from 'react-spinners/GridLoader';

import NewsHolder from './NewsHolder';

import * as a from '../state/actions';

const override = css`
  display: block;
  margin: 0 auto;
  border-color: red;
`;
const sweetPosition = {
  position: 'absolute',
  top: '50%',
  left: '50%',
  transform: 'translate("-50px", "-50px")'
};

const SpecificCategoryContainer = styled.div`
  display: grid;
  grid-template-areas:
    'header'
    'main';
  grid-template-columns: 1fr;
  > h1 {
    grid-area: header;
  }
`;
const SpecificCategory = ({ match }) => {
  const dispatch = useDispatch();
  const topNewsReducer = useSelector(({ topNewsReducer }) => topNewsReducer);
  const countryReducer = useSelector(({ countryReducer }) => countryReducer);
  const { isLoading, articles } = topNewsReducer;
  const { selectedCountry } = countryReducer;
  const { category } = match.params;

  useEffect(() => {
    dispatch(a.getCategoryHeadlinesAll(category, selectedCountry.code));
  }, [category, selectedCountry, dispatch]);

  if (isLoading) {
    return (
      <div className="sweet-loading" style={sweetPosition}>
        <GridLoader
          css={override}
          size={20}
          color={'rgb(216, 29, 216)'}
          loading={isLoading}
        />
      </div>
    );
  }

  return (
    <SpecificCategoryContainer>
      <h1>
        Top {category} news from {selectedCountry.name}:
      </h1>
      {articles.length === 0 ? (
        <h2>No data available</h2>
      ) : (
        <NewsHolder articles={articles} />
      )}
    </SpecificCategoryContainer>
  );
};

export default SpecificCategory;
