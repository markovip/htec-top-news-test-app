import React from 'react';
import renderer from 'react-test-renderer';
import Dummy from './Dummy';

it('renders correctly', () => {
  const tree = renderer.create(<Dummy />).toJSON();
  expect(tree).toMatchSnapshot();
});
