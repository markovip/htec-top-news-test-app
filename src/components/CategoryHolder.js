import React from 'react';
import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import NewsCard from './NewsCard';

const CategoryPanel = styled.div`
  display: grid;
  grid-template-areas:
    'header'
    'main';
  grid-template-columns: 1fr;
  > a {
    grid-area: header;
    padding-left: 15px;
    padding-top: 20px;
  }
`;

const SpecificCategoryPanel = styled.div`
  display: grid;
  grid-template-columns: repeat(auto-fill, minmax(250px, 1fr));
  grid-auto-rows: auto;
  padding: 1rem;
  grid-gap: 1rem;
`;
const CategoryHolder = ({ category, articles }) => {
  console.log(articles);
  return (
    <CategoryPanel>
      <Link to={`/categories/${category}`}>{category}</Link>

      <SpecificCategoryPanel>
        {articles.map(({ title, description, urlToImage, url }, index) => (
          <NewsCard
            key={index}
            title={title}
            description={description}
            imageUrl={urlToImage}
            linkTo={encodeURIComponent(title)}
            showLink={false}
          />
        ))}
      </SpecificCategoryPanel>
    </CategoryPanel>
  );
};

CategoryHolder.propTypes = {
  /** Category name */
  category: PropTypes.string,
  /** List of category articles */
  articles: PropTypes.array
};

CategoryHolder.defaultProps = {
  category: 'Category name',
  articles: []
};

export default CategoryHolder;
