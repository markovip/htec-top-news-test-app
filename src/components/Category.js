import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import styled from 'styled-components';
import { css } from '@emotion/core';
import GridLoader from 'react-spinners/GridLoader';

import CategoryHolder from './CategoryHolder';

import * as a from '../state/actions';
import { CATEGORIES } from '../state/constants';

const override = css`
  display: block;
  margin: 0 auto;
  border-color: red;
`;
const sweetPosition = {
  position: 'absolute',
  top: '50%',
  left: '50%',
  transform: 'translate("-50px", "-50px")'
};

const CategoryContainer = styled.div`
  display: grid;
  grid-template-areas:
    'header'
    'main';
  grid-template-columns: 1fr;
  > h1 {
    grid-area: header;
  }
`;

const Category = props => {
  const dispatch = useDispatch();
  const categoryNewsReducer = useSelector(
    ({ categoryNewsReducer }) => categoryNewsReducer
  );
  const countryReducer = useSelector(({ countryReducer }) => countryReducer);
  const { isLoading, categories } = categoryNewsReducer;
  const { selectedCountry } = countryReducer;
  console.log(categories);
  useEffect(() => {
    dispatch(a.getCategoryHeadlines(CATEGORIES, selectedCountry.code));
  }, [selectedCountry, dispatch]);

  if (isLoading) {
    return (
      <div className="sweet-loading" style={sweetPosition}>
        <GridLoader
          css={override}
          size={20}
          color={'rgb(216, 29, 216)'}
          loading={isLoading}
        />
      </div>
    );
  }

  return (
    <CategoryContainer>
      <h1>Top 5 news by categories from {selectedCountry.name}:</h1>
      {categories.map((current, index) => (
        <CategoryHolder
          key={index}
          category={current.category}
          articles={current.response.articles}
        />
      ))}
    </CategoryContainer>
  );
};

export default Category;
