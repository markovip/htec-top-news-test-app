import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';

const StyledDummyPanel = styled.div`
  width: 300px;
  height: 300px;
  background-color: #f00;
`;
const Dummy = ({ text }) => (
  <StyledDummyPanel>
    <h1>{text}</h1>
  </StyledDummyPanel>
);

Dummy.propTypes = {
  /** News Card title */
  text: PropTypes.string
};

Dummy.defaultProps = {
  text: 'Text goes here'
};

export default Dummy;
