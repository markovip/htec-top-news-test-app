import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import styled from 'styled-components';

const Article = styled.article`
  display: grid;
  // box-shadow: 0px 1px 5px #555;
  background-color: white;
  border-radius: 5px;
  overflow: hidden;
  box-shadow: 5px 5px 10px rgba(0, 0, 0, 0.3);

  .card__title {
    font-size: 1rem;
    padding: 0.5rem;

    h3 {
      color: black;
    }
  }
  .card__description {
    padding: 0.5rem;
    line-height: 1.6em;
    color: black;
  }

  .button {
    display: block;
    background-color: rgb(216, 29, 216);
    padding: 10px 20px;
    color: white;
    text-decoration: none;
    text-align: center;
    transition: 0.3s ease-out;
    align-self: end;
    > &:hover {
      background-color: darken(rgb(216, 29, 216), 10%);
    }
  }
  img {
    max-width: 100%;
    display: block;
    margin-left: auto;
    margin-right: auto;
    ${({ linkTo }) => linkTo && linkTo.length && 'width: 50%;'}
  }
`;
const NewsCard = ({ title, description, imageUrl, linkTo, showLink }) => (
  <Article className="card">
    <header className="card__title">
      <h3>{title}</h3>
    </header>
    <figure className="card__thumbnail">
      <img src={imageUrl} />
    </figure>
    <main className="card__description">{description}</main>
    {showLink ? (
      <Link to={`/${linkTo}`} className="button">
        {linkTo && linkTo.length > 0 ? 'Read More...' : 'Back to news'}
      </Link>
    ) : null}
  </Article>
);

NewsCard.propTypes = {
  /** News Card title */
  title: PropTypes.string,
  /** News Card description */
  description: PropTypes.string,
  /** News Card image */
  imageUrl: PropTypes.string,
  linkTo: PropTypes.string,
  showLink: PropTypes.bool
};

NewsCard.defaultProps = {
  title: 'Title goes here',
  description: 'Description goes here',
  imageUrl: '',
  linkTo: '',
  showLink: true
};

export default NewsCard;
