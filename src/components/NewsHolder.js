import React from 'react';
import NewsCard from './NewsCard';

import styled from 'styled-components';

const Panel = styled.div`
  display: grid;
  grid-template-columns: repeat(auto-fill, minmax(320px, 1fr));
  grid-auto-rows: auto;
  padding: 1rem;
  grid-gap: 1rem;
`;

// eslint-disable-next-line react/prop-types
const NewsHolder = ({ articles }) => {
  return (
    <Panel>
      {articles &&
        // eslint-disable-next-line react/prop-types
        articles.map((current, index) => (
          <NewsCard
            key={index}
            title={current.title}
            imageUrl={current.urlToImage}
            description={current.description}
            linkTo={encodeURIComponent(current.title)}
          />
        ))}
    </Panel>
  );
};

export default NewsHolder;
