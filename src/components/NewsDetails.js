/* eslint-disable react/prop-types */
import React from 'react';
import { useSelector } from 'react-redux';
import NewsCard from './NewsCard';

const NewsDetail = ({ match }) => {
  const topNewsReducer = useSelector(({ topNewsReducer }) => topNewsReducer);

  const { articles } = topNewsReducer;

  const article = articles.find(article => {
    console.log(encodeURIComponent(article.title));
    console.log(match.params.title);
    return (
      encodeURIComponent(article.title) ===
      encodeURIComponent(match.params.title)
    );
  });

  return (
    <NewsCard
      title={article.title}
      description={article.content}
      imageUrl={article.urlToImage}
      linkTo=""
    />
  );
};

export default NewsDetail;
