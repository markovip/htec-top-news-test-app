import React, { useState, useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import styled from 'styled-components';

import NewsHolder from './NewsHolder';
import * as a from '../state/actions';

const SearchContainer = styled.div`
  display: grid;
  grid-template-areas:
    'header'
    'search'
    'main';
  grid-template-columns: 1fr;
  > h1 {
    grid-area: header;
  }
  > form {
    grid-area: search;
    padding-left: 5px;

    > input {
      width: 97%;
      padding: 12px;
      margin: 10px;
      border: 1px solid #ccc;
      border-radius: 4px;
      resize: vertical;
      font-family: 'Montserrat';

      @media (max-width: 600px) {
        width: 90%;
      }
    }
  }
`;

export default function Search () {
  const dispatch = useDispatch();
  const [inputValue, setInputValue] = useState('');

  const countryReducer = useSelector(({ countryReducer }) => countryReducer);
  const { selectedCountry } = countryReducer;

  const topNewsReducer = useSelector(({ topNewsReducer }) => topNewsReducer);
  const { articles } = topNewsReducer;

  function handleInputChange (e) {
    setInputValue(e.target.value);
  }

  useEffect(() => {
    dispatch(a.searchHeadlines(inputValue, selectedCountry.code));
  }, [inputValue, selectedCountry, dispatch]);

  return (
    <SearchContainer>
      <h1>Search Top News from {selectedCountry.name} by term:</h1>
      <form>
        <input
          placeholder="Search term..."
          value={inputValue}
          onChange={handleInputChange}
        />
      </form>
      <NewsHolder articles={articles} />
    </SearchContainer>
  );
}
