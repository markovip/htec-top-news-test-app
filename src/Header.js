import React from 'react';
import { NavLink } from 'react-router-dom';

import CountrySelector from './components/CountrySelector';

import './styles/header';

const Header = () => {
  return (
    <nav className="grid">
      <ul>
        <li>
          <NavLink exact to="/" activeClassName="selected">
            Top News
          </NavLink>
        </li>
        <li>
          <NavLink to="/categories" activeClassName="selected">
            Category
          </NavLink>
        </li>
        <li>
          <NavLink to="/search" activeClassName="selected">
            Search
          </NavLink>
        </li>
        <CountrySelector />
      </ul>
    </nav>
  );
};

export default Header;
